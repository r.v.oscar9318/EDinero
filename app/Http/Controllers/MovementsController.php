<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StoreMovement;
use App\Movement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class MovementsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Movimientos';
        $movements = Movement::where('user_id', auth()->user()->id);

        if ($request->has('type')) {
            $movements = $movements->where('type', $request->get('type'));
            $title = 'Movimientos de ' . $request->get('type');
        }

        $movements = $movements->orderBy('movement_date', 'desc')->paginate();

        return view('movements.index', compact('movements', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name')->pluck('name', 'id');

        return view('movements.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMovement|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMovement $request)
    {
        // Es equivalente a (new Movement)->fill($request()->all());
        $movement = new Movement($request->all());
        $movement->money = $request->get('money_decimal') * 100;

        $category = $request->get('category_id');

        // Asegura la existencia de la categoría en la base de datos
        if (!is_numeric($category)) {
            $newCategory = Category::firstOrCreate(['name' => ucwords($category)]);
            $movement->category_id = $newCategory->id;
        }

        // Asigna el ID del usuario en sesión
        $movement->user_id = auth()->user()->id;

        if ($request->hasFile('image')) {
            // Archivo del input type file con name "image"
            $image = $request->file('image');

            /*
            // Para usar un nombre de archivo personalizado se usa storeAs() en lugar de store()
            $filepath = str_slug($image->getClientOriginalName())
                . date('YmdHis.')
                . $image->extension();
            $file = $image->storeAs('images/movements', $filepath);
            */

            // Sube la imagen a la carpeta public/images/movements
            $file = $image->store('images/movements');

            // Guarda la ruta de la imagen en el campo image
            $movement->image = $file;
        }

        // Guarda en la base de datos
        $movement->save();

        return redirect()->route('movements.show', $movement);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movement = Movement::where('user_id', auth()->user()->id)
            ->where('id', $id)
            ->first();

        return view('movements.show', compact('movement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::orderBy('name')->pluck('name', 'id');

        $movement = Movement::where('user_id', auth()->user()->id)
            ->where('id', $id)
            ->first();

        return view('movements.edit', compact('categories', 'movement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreMovement|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMovement $request, $id)
    {
        $movement = Movement::where('user_id', auth()->user()->id)
            ->where('id', $id)
            ->first();

        $movement->type = $request->get('type');
        $movement->movement_date = $request->get('movement_date');
        $movement->money = $request->get('money_decimal') * 100;
        $movement->description = $request->get('description');

        $category = $request->get('category_id');

        // Asegura la existencia de la categoría en la base de datos
        if (!is_numeric($category)) {
            $newCategory = Category::firstOrCreate(['name' => ucwords($category)]);
            $movement->category_id = $newCategory->id;
        }

        if ($request->hasFile('image')) {
            // Archivo del input type file con name "image"
            $image = $request->file('image');

            // Sube la imagen a la carpeta public/images/movements
            $file = $image->store('images/movements');

            // Guarda la ruta de la imagen en el campo image
            $movement->image = $file;
        }

        // Guarda en la base de datos
        $movement->save();

        return redirect()->route('movements.show', $movement);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
